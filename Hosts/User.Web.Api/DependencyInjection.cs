﻿using System.Reflection;
using Libs.Helpers;
using Newtonsoft.Json;

namespace User.Web.Api;

public static class DependencyInjection
{
    public static IConfigurationBuilder AutoSetJsonFiles(this IConfigurationBuilder configurationBuilder)
    {
        /* set shared json file */
        var sharedJsonFileRoot = Path.Combine(DirectoryHelper.Current, "..", "..", "Core", "Configs", "SharedSettings", "Json");
        var sharedJsonFileList = DirectoryHelper.GetFileNameList(sharedJsonFileRoot, fileFormat: "json");
        
        configurationBuilder.SetBasePath(sharedJsonFileRoot);
        sharedJsonFileList.ForEach(fileName => configurationBuilder.AddJsonFile(fileName, optional: true));
        
        /* set project json file */
        var projectName = Assembly.GetCallingAssembly().GetName().Name;
        var projectJsonFileRoot = DirectoryHelper.GetParentPath(Path.Combine(projectName!, "Settings", "Json"));
        var projectJsonFileList = DirectoryHelper.GetFileNameList(projectJsonFileRoot, fileFormat: "json");
        
        configurationBuilder.SetBasePath(projectJsonFileRoot);
        projectJsonFileList.ForEach(fileName => configurationBuilder.AddJsonFile(fileName, optional: true));

        return configurationBuilder;
    }
}