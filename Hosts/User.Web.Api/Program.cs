using System.Reflection;
using Configs;
using Configs.Middlewares;
using Libs.Helpers;
using Libs.Wrappers;
using Microsoft.AspNetCore.HttpOverrides;
using User.Web.Api;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration.AutoSetJsonFiles().AddEnvironmentVariables().Build();

// Set services to the container.
var services = builder.Services;
services.SetCoreServiceLayer();

// Set http request pipeline.
var app = builder.Build();
app.UseCors(x => x
    .SetIsOriginAllowed(_ => true)
    .AllowAnyMethod()
    .AllowAnyHeader()
    .AllowCredentials());

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

app.UseMiddleware<ResponseMiddleware>();
app.UseRouting();
app.UseEndpoints(endpoints => endpoints.MapControllers());

// Default endpoint
app.MapGet("/", (HttpContext context) => new Response(
    name: configuration.GetValue<string>("ApplicationSettings:Name"),
    message: "API is running",
    data: $"You are accessing this endpoint from {IdentityHelper.GetClientIp(context)}"
));
app.Run();