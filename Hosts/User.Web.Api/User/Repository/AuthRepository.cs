﻿using NetCore.AutoRegisterDi;

namespace User.Web.Api.User.Repository;

public interface IAuthRepository
{
    void GenerateDevice();
}

[RegisterAsScoped]
public class AuthRepository : IAuthRepository
{
    public void GenerateDevice()
    {
    }
}