﻿using Microsoft.AspNetCore.Mvc;

namespace User.Web.Api.User.Handler;

[ApiController]
[Route("account")]
public class AccountHandler : ControllerBase
{
    [HttpPost] public IActionResult Profile() => Ok();
}