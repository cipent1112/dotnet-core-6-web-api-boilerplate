﻿using System.Net;
using Libs.Enums;
using Libs.Exceptions;
using Libs.Helpers;
using Libs.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Configs.Middlewares;

public class ResponseMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<ResponseMiddleware> _logger;

    public ResponseMiddleware(RequestDelegate next, ILogger<ResponseMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var watch = StopwatchHelper.StartNew();
        var request = ResponseHelper.FormatRequest(context.Request);
        var originalBodyStream = context.Response.Body;

        using var bodyStream = new MemoryStream();
        try
        {
            context.Response.Body = bodyStream;
            await _next.Invoke(context);
            context.Response.Body = originalBodyStream;

            var successStatusList = new[] { (int)HttpStatusCode.OK, (int)HttpStatusCode.Created };
            if (successStatusList.Contains(context.Response.StatusCode))
            {
                var bodyAsText = await ResponseHelper.FormatResponse(bodyStream);
                var requestTime = watch.GetElapsedTime();
                await HandleSuccessRequestAsync(context, bodyAsText, context.Response.StatusCode, requestTime);
            }
            else await HandleNotSuccessRequestAsync(context, context.Response.StatusCode);
        }
        catch (Exception ex)
        {
            _logger.LogCritical("Error: {ErrorMessage}", ex.Message);
            await HandleExceptionAsync(context, ex);
            bodyStream.Seek(0, SeekOrigin.Begin);
            await bodyStream.CopyToAsync(originalBodyStream);
        }
        finally
        {
            _logger.LogInformation(
                "Request: {@Request} Responded with [{ResponseStatusCode}] in {StopWatchElapsedMilliseconds}",
                request, context.Response.StatusCode, watch.GetElapsedTime());
        }
    }

    private static Task HandleExceptionAsync(HttpContext context, Exception ex)
    {
        var message = ResponseMessageEnum.Unhandled.GetDescription();
        var type = typeof(SystemException).ToString();
        var code = (int)ResponseCodeEnum.Failed;
        var status = (int)HttpStatusCode.InternalServerError;
        object? data = null;
        
        switch (ex)
        {
            case HttpException httpException:
                message = httpException.Message;
                code = httpException.Code;
                status = httpException.Status;
                data = httpException.Data ?? null;
                type = typeof(HttpException).ToString();
                break;
            case ValidationException validationException:
                message = validationException.Message;
                code = validationException.Code;
                status = validationException.Status;
                data = validationException.Errors;
                type = typeof(ValidationException).ToString();
                break;
            case HttpRequestException httpRequestException:
                message = httpRequestException.Message;
                status = httpRequestException.StatusCode.GetHashCode();
                type = typeof(HttpRequestException).ToString();
                break;
        }
        
        context.Response.ContentType = "application/json";
        context.Response.StatusCode = status;
        
        return context.Response.WriteAsync(ConvertToJsonString(
                new Response(
                    message: message,
                    code: code,
                    status: status,
                    data: data
                ) { Type = type }
            )
        );
    }

    private static Task HandleSuccessRequestAsync(HttpContext context, object body, int status, string requestTime)
    {
        string jsonString;
        var bodyText = !body.ToString()!.IsValidJson() ? ConvertToJsonString(body) : body.ToString();
        var bodyContent = JsonConvert.DeserializeObject<dynamic>(bodyText!);
        Type type = bodyContent?.GetType()!;

        if (type == typeof(JObject))
        {
            var response = JsonConvert.DeserializeObject<Response>(bodyText!);
            if (response != null && (response.Status != status || response.Data != null ||
                                     (response.Status == status && response.Data == null)))
                jsonString = ConvertToJsonString(response, requestTime);
            else jsonString = ConvertToJsonString(status, bodyContent, requestTime);
        }
        else jsonString = ConvertToJsonString(status, bodyContent, requestTime);

        context.Response.ContentType = "application/json";
        return context.Response.WriteAsync(jsonString);
    }

    private static Task HandleNotSuccessRequestAsync(HttpContext context, int status)
    {
        var message = ResponseMessageEnum.Unknown.GetDescription();
        var type = "Unknown";

        switch (status)
        {
            case (int)HttpStatusCode.NotFound:
                message = ResponseMessageEnum.NotFound.GetDescription();
                type = typeof(KeyNotFoundException).ToString();
                break;
            case (int)HttpStatusCode.NoContent:
                message = ResponseMessageEnum.NotContent.GetDescription();
                type = typeof(NoContentResult).ToString();
                break;
            case (int)HttpStatusCode.MethodNotAllowed:
                message = ResponseMessageEnum.MethodNotAllowed.GetDescription();
                type = typeof(MethodAccessException).ToString();
                break;
        }

        context.Response.ContentType = "application/json";
        context.Response.StatusCode = status;

        return context.Response.WriteAsync(ConvertToJsonString(
                new Response(
                    message: message,
                    code: (int)ResponseCodeEnum.Failed,
                    status: status
                ) { Type = type }
            )
        );
    }

    private static string ConvertToJsonString(int status, object data, string requestTime)
    {
        return JsonConvert.SerializeObject(new Response(
                status: status,
                data: data
            ) { RequestTime = requestTime },
            ResponseHelper.JsonSettings());
    }

    private static string ConvertToJsonString(Response response, string requestTime)
    {
        response.RequestTime = requestTime;
        return JsonConvert.SerializeObject(response, ResponseHelper.JsonSettings());
    }

    private static string ConvertToJsonString(Response response)
    {
        return JsonConvert.SerializeObject(response, ResponseHelper.JsonSettings());
    }

    private static string ConvertToJsonString(object rawJson)
    {
        return JsonConvert.SerializeObject(rawJson, ResponseHelper.JsonSettings());
    }
}