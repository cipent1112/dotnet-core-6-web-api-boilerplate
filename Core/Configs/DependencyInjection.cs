﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Configs;

public static class DependencyInjection
{
    public static void SetCoreServiceLayer(this IServiceCollection service)
    {
        SetController(service);
    }

    private static void SetController(this IServiceCollection service)
    {
        service.AddControllers().AddNewtonsoftJson();
        JsonConvert.DefaultSettings = () => new JsonSerializerSettings
        {
            ContractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy
                {
                    OverrideSpecifiedNames = false
                }
            }
        };
        service.AddRouting(options => options.LowercaseUrls = true);
    }
}