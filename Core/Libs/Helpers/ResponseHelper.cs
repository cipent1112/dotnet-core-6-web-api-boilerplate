﻿using System.Text;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Libs.Helpers;

public static class ResponseHelper
{
    public static bool IsValidJson(this string text)
    {
        text = text.Trim();
        if ((!text.StartsWith("{") || !text.EndsWith("}")) && (!text.StartsWith("[") || !text.EndsWith("]")))
            return false;
        try
        {
            var obj = JToken.Parse(text);
            return true;
        }
        catch (JsonReaderException)
        {
            return false;
        }
        catch (Exception)
        {
            return false;
        }
    }
    
    public static async Task<string> FormatRequest(HttpRequest request)
    {
        request.EnableBuffering();

        var buffer = new byte[Convert.ToInt32(request.ContentLength)];
        await request.Body.ReadAsync(buffer, 0, buffer.Length);
        var bodyAsText = Encoding.UTF8.GetString(buffer);
        request.Body.Seek(0, SeekOrigin.Begin);

        return $"{request.Method} {request.Scheme} {request.Host}{request.Path} {request.QueryString} {bodyAsText}";
    }
    
    public static async Task<string> FormatResponse(Stream bodyStream)
    {
        bodyStream.Seek(0, SeekOrigin.Begin);
        var plainBodyText = await new StreamReader(bodyStream).ReadToEndAsync();
        bodyStream.Seek(0, SeekOrigin.Begin);

        return plainBodyText;
    }
    
    public static JsonSerializerSettings JsonSettings() => new()
    {
        ContractResolver = new CamelCasePropertyNamesContractResolver(),
        Converters = new List<JsonConverter> { new StringEnumConverter() }
    };
}