﻿using System.Diagnostics;

namespace Libs.Helpers;

public readonly struct StopwatchHelper
{
    private static readonly double STimestampToTicks = TimeSpan.TicksPerSecond / (double)Stopwatch.Frequency;
    private readonly long _startTimestamp;
    private StopwatchHelper(long startTimestamp) => _startTimestamp = startTimestamp;
    public static StopwatchHelper StartNew() => new(GetTimestamp());
    private static long GetTimestamp() => Stopwatch.GetTimestamp();

    private static TimeSpan GetElapsedTime(long startTimestamp, long endTimestamp)
    {
        var timestampDelta = endTimestamp - startTimestamp;
        var ticks = (long)(STimestampToTicks * timestampDelta);
        return new TimeSpan(ticks);
    }

    public string GetElapsedTime() =>
        $"{Math.Round(GetElapsedTime(_startTimestamp, GetTimestamp()).TotalMilliseconds, 2)}".Replace(",", ".") + "ms";
}