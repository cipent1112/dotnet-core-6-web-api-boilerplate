﻿using Libs.Enums;
using Microsoft.AspNetCore.WebUtilities;

namespace Libs.Exceptions;

public class HttpException : Exception
{
    public string Name { get; set; }
    public new string Message { get; set; }
    public int Code { get; set; }
    public int Status { get; set; }
    public new object? Data { get; set; }

    public HttpException(int status, string message, object? data = null,
        int code = (int)ResponseCodeEnum.Failed) : base(message)
    {
        Name = ReasonPhrases.GetReasonPhrase(status);
        Message = message;
        Code = code;
        Status = status;
        Data = data;
    }
}