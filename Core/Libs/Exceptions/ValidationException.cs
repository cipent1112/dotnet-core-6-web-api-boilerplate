﻿using System.Net;
using FluentValidation.Results;
using Libs.Enums;
using Libs.Helpers;

namespace Libs.Exceptions;

public class ValidationException : Exception
{
    public int Code { get; set; }
    public int Status { get; set; }
    public IDictionary<string, string[]> Errors { get; }

    public ValidationException() : base(ResponseMessageEnum.ValidationError.GetDescription())
    {
        Errors = new Dictionary<string, string[]>();
        Code = (int)ResponseCodeEnum.Failed;
        Status = (int)HttpStatusCode.BadRequest;
    }

    public ValidationException(IReadOnlyCollection<ValidationFailure> failures)
        : this()
    {
        var propertyNames = failures
            .Select(e => e.PropertyName)
            .Distinct();

        foreach (var propertyName in propertyNames)
        {
            var propertyFailures = failures
                .Where(e => e.PropertyName == propertyName)
                .Select(e => e.ErrorMessage)
                .ToArray();

            Errors.Add(propertyName, propertyFailures);
        }
    }
}