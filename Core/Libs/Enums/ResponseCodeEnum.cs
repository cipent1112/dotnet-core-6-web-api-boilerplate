﻿namespace Libs.Enums;

public enum ResponseCodeEnum: int
{
    Success = 10001,
    Failed = 10002
}