﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using Libs.Enums;
using Libs.Helpers;
using Microsoft.AspNetCore.WebUtilities;

namespace Libs.Wrappers;

[DataContract]
public class Response
{
    [DataMember] public string Name { get; set; }
    [DataMember] public string Message { get; set; }
    [DataMember] public int Code { get; set; }
    [DataMember] public int Status { get; set; }
    [DataMember(EmitDefaultValue = false)] public string? Type { get; set; }
    [DataMember(EmitDefaultValue = false)] public string? RequestTime { get; set; }
    [DataMember(EmitDefaultValue = false)] public object? Data { get; set; }
    [DataMember(EmitDefaultValue = false)] public object? Meta { get; set; }

    [JsonConstructor]
    public Response(
        object? data = null,
        string? name = null,
        string? message = null,
        int code = (int)ResponseCodeEnum.Success,
        int status = 200
    )
    {
        Name = name ?? ReasonPhrases.GetReasonPhrase(status);
        Message = message ?? ResponseMessageEnum.Success.GetDescription();
        Code = code;
        Status = status;
        Data = data;
    }
}